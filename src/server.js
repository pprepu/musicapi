import express from "express";
import artistRouter from "./routers/artistRouter.js";
import albumRouter from "./routers/albumRouter.js";

const server = express();

server.use(express.json());
server.use("/artists", artistRouter);
server.use("/albums", albumRouter);

export default server;