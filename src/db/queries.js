// artists
const createArtistsTable = `CREATE TABLE IF NOT EXISTS "artists" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(100) NOT NULL
);`;

const getArtists = "SELECT id, name FROM artists;";

const getArtist = "SELECT id, name FROM artists WHERE id = $1;";

const createArtist = "INSERT INTO artists (name) VALUES ($1) RETURNING id;";

const deleteArtist = "DELETE from artists WHERE id = $1 RETURNING id;";

const updateArtist = "UPDATE artists SET name = $2 WHERE id = $1 RETURNING id;"

// albums
const createAlbumsTable = `CREATE TABLE IF NOT EXISTS "albums" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(100) NOT NULL,
  "artist" integer NOT NULL,
  "release_year" smallint NOT NULL
);`;

const getAlbums = "SELECT albums.id, albums.name, artists.name AS artist, albums.release_year FROM albums LEFT JOIN artists ON albums.artist = artists.id WHERE release_year >= $1 AND release_year <= $2;";

const getAlbum = "SELECT albums.id, albums.name, artists.name AS artist, albums.release_year FROM albums LEFT JOIN artists ON albums.artist = artists.id WHERE albums.id = $1;"

const createAlbum = "INSERT INTO albums (name, artist, release_year) VALUES ($1, $2, $3) RETURNING id;";

const deleteAlbum = "DELETE from albums WHERE id = $1 RETURNING id;";

const updateAlbum = "UPDATE albums SET name = $2, artist = $3, release_year = $4 WHERE id = $1 RETURNING id;";

export default {
  createArtistsTable,
  getArtists,
  getArtist,
  createArtist,
  deleteArtist,
  updateArtist,

  createAlbumsTable,
  getAlbums,
  getAlbum,
  createAlbum,
  deleteAlbum,
  updateAlbum,
}