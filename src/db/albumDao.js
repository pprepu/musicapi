import queries from "./queries.js";
import { executeQuery } from "./db.js";

const getAlbums = async (from, to) => {
  const defaultFrom = 1;
  const defaultTo = 32766;

  from = isNaN(from) ? defaultFrom : from;
  to = isNaN(to) ? defaultTo : to;
  
  const result = await executeQuery(queries.getAlbums, [from, to]);
  return result.rows;
};

const getAlbum = async id => {
  const result = await executeQuery(queries.getAlbum, [id]);
  return result.rows[0];
};

const createAlbum = async (name, artist, releaseYear)=> {
  const result = await executeQuery(queries.createAlbum, [name, artist, releaseYear]);
  return result.rows[0];
};

const deleteAlbum = async id => {
  const result = await executeQuery(queries.deleteAlbum, [id]);
  return result.rows[0];
};

const updateAlbum = async (id, name, artist, releaseYear) => {
  const result = await executeQuery(queries.updateAlbum, [id, name, artist, releaseYear]);
  return result.rows[0];
}

export default {
  getAlbums,
  getAlbum,
  createAlbum,
  deleteAlbum,
  updateAlbum,
}