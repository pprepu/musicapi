import queries from "./queries.js";
import { executeQuery } from "./db.js";

const getArtists = async () => {
  const result = await executeQuery(queries.getArtists);
  return result.rows;
};

const getArtist = async id => {
  const result = await executeQuery(queries.getArtist, [id]);
  return result.rows[0];
};

const createArtist = async name => {
  const result = await executeQuery(queries.createArtist, [name]);
  return result.rows[0];
};

const deleteArtist = async id => {
  const result = await executeQuery(queries.deleteArtist, [id]);
  return result.rows[0];
};

const updateArtist = async (id, name) => {
  const result = await executeQuery(queries.updateArtist, [id, name]);
  return result.rows[0];
}

export default {
  getArtists,
  getArtist,
  createArtist,
  deleteArtist,
  updateArtist,
}