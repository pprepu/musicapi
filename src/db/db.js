import pg from "pg";
import queries from "./queries.js";

const isProd = process.env.NODE_ENV === "production";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

export const pool = new pg.Pool({
  host: PG_HOST,
  port: PG_PORT,
  user: PG_USERNAME,
  password: PG_PASSWORD,
  database: PG_DATABASE,
  ssl: isProd
});

export const executeQuery = async(query, params) => {
  const client = await pool.connect();
  try {
    const result = await client.query(query, params);
    return result;
  } catch (error) {
    console.error(error.stack);
    error.name = "dbError";
    throw error;
  } finally {
    client.release();
  }
};

// for cloud db
export const createArtistsTable = async () => {
  await executeQuery(queries.createArtistsTable);
  console.log("artists table created");
};

export const createAlbumsTable = async () => {
  await executeQuery(queries.createAlbumsTable);
  console.log("albums table created");
};