import server from "./server.js";
import { createArtistsTable, createAlbumsTable } from "./db/db.js";

createArtistsTable();
createAlbumsTable();

const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
  console.log("listening to port", PORT);
});