import { Router } from "express";
import dao from "../db/albumDao.js";
import { validateId, validateAlbumFields, validateFromAndToRequestParameters } from "../middlewares.js";

const router = Router();

router.get("/", validateFromAndToRequestParameters, async (req, res) => {
  const from = Number(req.query.from);
  const to = Number(req.query.to);

  const albums = await dao.getAlbums(from, to);
  res.send(albums);
});

router.get("/:id", validateId, async (req, res) => {
  const id = Number(req.params.id);

  const album = await dao.getAlbum(id);
  if (!album) {
    return res.status(404).send("id not found");
  }
  res.send(album);
});

router.post("/", validateAlbumFields, async (req, res) => {
  const { name, artist, releaseYear } = req.body;

  const albumCreated = await dao.createAlbum(name, artist, releaseYear);
  res.status(201).send({ ...albumCreated, name, artist, releaseYear });
});

router.delete("/:id", validateId, async (req, res) => {
  const id = Number(req.params.id);

  const albumDeleted = await dao.deleteAlbum(id);
  if (!albumDeleted) {
    return res.status(404).send("id not found");
  }

  res.status(204).send();
});

router.put("/:id", validateId, validateAlbumFields, async (req, res) => {
  const id = Number(req.params.id);
  const { name, artist, releaseYear } = req.body;

  const albumUpdated = await dao.updateAlbum(id, name, artist, releaseYear);
  if (!albumUpdated) {
    return res.status(404).send("id not found");
  }
  res.status(201).send({ id, name, artist, releaseYear });
})

export default router;