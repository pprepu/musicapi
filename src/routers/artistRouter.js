import { Router } from "express";
import dao from "../db/artistDao.js";
import { validateId } from "../middlewares.js";

const router = Router();

router.get("/", async (req, res) => {
  const artists = await dao.getArtists();
  res.send(artists);
});

router.get("/:id", validateId, async (req, res) => {
  const id = Number(req.params.id);
  const artist = await dao.getArtist(id);
  if (!artist) {
    return res.status(404).send("id not found");
  }
  res.send(artist);
});

router.post("/", async (req, res) => {
  const { name } = req.body;
  if (!name) {
    return res.status(400).send("'name' field missing");
  }
  const artistCreated = await dao.createArtist(name);
  res.status(201).send({ ...artistCreated, name });
});

router.delete("/:id", validateId, async (req, res) => {
  const id = Number(req.params.id);

  const artistDeleted = await dao.deleteArtist(id);

  if (!artistDeleted) {
    return res.status(404).send("id not found");
  }

  res.status(204).send();
});

router.put("/:id", validateId, async (req, res) => {
  const id = Number(req.params.id);
  const { name } = req.body;
  if (!name) {
    return res.status(400).send("'name' field missing");
  }
  const artistUpdated = await dao.updateArtist(id, name);
  if (!artistUpdated) {
    return res.status(404).send("id not found");
  }
  res.status(201).send({ id, name });
})

export default router;