export const validateId = (req, res, next) => {
  const id = Number(req.params.id);
  if (isNaN(id)) {
    return res.status(404).send("invalid id");
  }

  next();
};

export const validateAlbumFields = (req, res, next) => {
  const { name, artist, releaseYear } = req.body;
  if (!name || !artist || !releaseYear) {
    return res.status(400).send("required fields missing");
  }

  if (isNaN(artist) || isNaN(releaseYear)) {
    return res.status(400).send("'artist' or 'releaseYear' could not be parsed into numbers");
  }

  next();
};

export const validateFromAndToRequestParameters = (req, res, next) => {
  const { from, to } = req.query;
  if (from !== undefined && ( isNaN(from) || from > 32767 || from < 1 )) {
    return res.status(400).send("from request parameter was sent, but invalid");
  }

  if (from !== undefined && !Number.isInteger(Number(from))) {
    return res.status(400).send("from request parameter must be integer");
  }

  if (to !== undefined && ( isNaN(to) || to > 32767 || to < 1 )) {
    return res.status(400).send("to request parameter was sent, but invalid");
  }

  if (to !== undefined && !Number.isInteger(Number(to))) {
    return res.status(400).send("to request parameter must be integer");
  }

  next();
};