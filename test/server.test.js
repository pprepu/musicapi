import { jest } from "@jest/globals";
import request from "supertest";
import server from "../src/server.js";
import { pool } from "../src/db/db.js";

// /artists:
describe("/artists route:", () => {
  describe("GET /artists", () => {
    const mockResponse = {
      rows: [
        { id: 1, name: "Artisti"},
        { id: 2, name: "Testaaja"}
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 200 with all artists", async () => {
      const response = await request(server).get("/artists");
      expect(response.status).toBe(200);
      expect(response.body).toEqual(mockResponse.rows);
    });
  });
  
  describe("GET /artists/id", () => {
    const mockResponse = {
      rows: [
        { id: 1, name: "Artisti" }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("with a valid id, returns status code 200 with an artist", async () => {
      const response = await request(server).get("/artists/1");
      expect(response.status).toBe(200);
      expect(response.body).toEqual(mockResponse.rows[0]);
    });
  
    it("with invalid id, returns status code 404 and an error text", async () => {
      const response = await request(server).get("/artists/numberone");
      expect(response.status).toBe(404);
      expect(response.text).toEqual("invalid id");
    });
  });
  
  describe("POST /artists", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 201 with the created artist", async () => {
      const newArtist = { name: "newArtist" };
      const response = await request(server).post("/artists").send(newArtist);
  
      expect(response.status).toBe(201);
      expect(response.body).toEqual({...mockResponse.rows[0], ...newArtist });
    });
  
    it("returns status code 400 with an error message if 'name' field is missing", async () => {
      const newArtist = { artist_name: "field is not correct" };
      const response = await request(server).post("/artists").send(newArtist);
      expect(response.status).toBe(400);
      expect(response.text).toBe("'name' field missing");
    });
  });
  
  describe("PUT /artists/id", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 201 with the updated artist", async () => {
      const updatedArtist = { name: "updated Artist" };
      const response = await request(server).put("/artists/1").send(updatedArtist);
      expect(response.status).toBe(201);
      expect(response.body).toEqual({...mockResponse.rows[0], ...updatedArtist });
    });
  
    it("returns status code 400 with an error message if 'name' field is missing", async () => {
      const updatedArtist = { artist_name: "field is not correct" };
      const response = await request(server).put("/artists/1").send(updatedArtist);
      expect(response.status).toBe(400);
      expect(response.text).toBe("'name' field missing");
    });
  });
  
  describe("DELETE /artists/id", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 204 with an empty response", async () => {
      const response = await request(server).delete("/artists/1");
      expect(response.status).toBe(204);
      expect(response.text).toBe("");
    });
  });
});

// /albums
describe("/albums route:", () => {
  describe("GET /albums", () => {
    const mockResponse = {
      rows: [
        { id: 1, name: "album", artist: 1, release_year: 2008 },
        { id: 2, name: "album 2" , artist: 1, release_year: 2015 },
        { id: 3, name: "album 3" , artist: 2, release_year: 2022 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
    it("returns status code 200 with all albums", async () => {
      const response = await request(server).get("/albums");
      expect(response.status).toBe(200);
      expect(response.body).toEqual(mockResponse.rows);
    });
    it("returns status code 400 with an error text if request parameter 'from' is bigger than smallint allows", async () => {
      const response = await request(server).get("/albums?from=32768");
      expect(response.status).toBe(400);
      expect(response.text).toEqual("from request parameter was sent, but invalid");
    });
    it("returns status code 400 with an error text if request parameter 'to' is bigger than smallint allows", async () => {
      const response = await request(server).get("/albums?to=32768");
      expect(response.status).toBe(400);
      expect(response.text).toEqual("to request parameter was sent, but invalid");
    });
    it("returns status code 400 with an error text if request parameter 'from' is a float number", async () => {
      const response = await request(server).get("/albums?from=3.3");
      expect(response.status).toBe(400);
      expect(response.text).toEqual("from request parameter must be integer");
    });
    it("returns status code 400 with an error text if request parameter 'to' is a float number", async () => {
      const response = await request(server).get("/albums?to=2010.5");
      expect(response.status).toBe(400);
      expect(response.text).toEqual("to request parameter must be integer");
    });
  });
  
  describe("GET /albums/id", () => {
    const mockResponse = {
      rows: [
        { id: 1, name: "album", artist: 1, release_year: 2008 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("with a valid id, returns status code 200 with an album", async () => {
      const response = await request(server).get("/albums/1");
      expect(response.status).toBe(200);
      expect(response.body).toEqual(mockResponse.rows[0]);
    });
  
    it("with invalid id, returns status code 404 and an error text", async () => {
      const response = await request(server).get("/albums/id");
      expect(response.status).toBe(404);
      expect(response.text).toEqual("invalid id");
    });
  });
  
  describe("POST /albums", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 201 with the created album", async () => {
      const newAlbum = { name: "album", artist: 1, releaseYear: 2008 };
      const response = await request(server).post("/albums").send(newAlbum);
  
      expect(response.status).toBe(201);
      expect(response.body).toEqual({...mockResponse.rows[0], ...newAlbum });
    });
  
    it("returns status code 400 with an error message if 'name' field is missing", async () => {
      const newAlbum = { artist: 1, releaseYear: 2008 };
      const response = await request(server).post("/albums").send(newAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("required fields missing");
    });
  
    it("returns status code 400 with an error message if 'artist' field is missing", async () => {
      const newAlbum = { name: "album", releaseYear: 2008 };
      const response = await request(server).post("/albums").send(newAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("required fields missing");
    });
  
    it("returns status code 400 with an error message if 'releaseYear' field is missing", async () => {
      const newAlbum = { name: "album", artist: 1 };
      const response = await request(server).post("/albums").send(newAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("required fields missing");
    });
  
    it("returns status code 400 with an error message if 'artist' field cannot be parsed to number", async () => {
      const newAlbum = { name: "album", artist: "Artisti", releaseYear: 2008 };
      const response = await request(server).post("/albums").send(newAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("'artist' or 'releaseYear' could not be parsed into numbers");
    });
  
    it("returns status code 400 with an error message if 'releaseYear' field cannot be parsed to number", async () => {
      const newAlbum = { name: "album", artist: 1, releaseYear: "2k21" };
      const response = await request(server).post("/albums").send(newAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("'artist' or 'releaseYear' could not be parsed into numbers");
    });
  });
  
  describe("PUT /albums/id", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
  
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 201 with the updated album", async () => {
      const updatedAlbum = { name: "updated album", artist: 1, releaseYear: 2010 };
      const response = await request(server).put("/albums/1").send(updatedAlbum);
      expect(response.status).toBe(201);
      expect(response.body).toEqual({...mockResponse.rows[0], ...updatedAlbum });
    });
  
    it("returns status code 400 with an error message if 'name' field is missing", async () => {
      const updatedAlbum = { artist: 1, releaseYear: 2010 };
      const response = await request(server).put("/albums/1").send(updatedAlbum);
      expect(response.status).toBe(400);
      expect(response.text).toBe("required fields missing");
    });
  });
  
  describe("DELETE /albums/id", () => {
    const mockResponse = {
      rows: [
        { id: 1 }
      ]
    };
    beforeEach(() => {
      pool.connect = jest.fn(() => {
        return {
          query: () => mockResponse,
          release: () => null
        };
      });
    });
  
    it("returns status code 204 with an empty response", async () => {
      const response = await request(server).delete("/albums/1");
      expect(response.status).toBe(204);
      expect(response.text).toBe("");
    });
  });
})
